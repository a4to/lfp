#!/usr/share/lfp/lfprc

# Basic Variables:

set icons
set period 1
set shell bash
set scrolloff 10
set drawbox true
set hiddenfiles ".*:*.aux:*.bbl:*.bcf:*.blg:*.lock:*-lock.json:*-lock.yaml:*.egg-info:*.run.xml:node_modules:__pycache__"
set shellopts '-eu'
set ifs "\n"


# Environment Variables:

$lf -remote "send $id set previewer /usr/share/lfp/scope"
$lf -remote "send $id set cleaner /usr/share/lfp/cleaner"

# Functions:

cmd open ${{
    case $(file --mime-type $f -b) in
	image/vnd.djvu|application/pdf|application/octet-stream) setsid -f zathura $fx >/dev/null 2>&1 ;;
  text/*) $EDITOR $fx;;
	image/x-xcf) setsid -f gimp $f >/dev/null 2>&1 ;;
	image/svg+xml) display -- $f ;;
	image/*) rotdir $f | grep -i "\.\(png\|jpg\|jpeg\|gif\|webp\|tif\|ico\)\(_large\)*$" | sxiv -aio 2>/dev/null | lf-select ;;
	audio/*) mpv --audio-display=no $f ;;
	video/*) setsid -f mpv $f -quiet >/dev/null 2>&1 ;;
	application/pdf|application/vnd*|application/epub*) setsid -f zathura $fx >/dev/null 2>&1 ;;
	application/pgp-encrypted) $EDITOR $fx ;;
        *) for f in $fx; do setsid -f $OPENER $f >/dev/null 2>&1; done;;
    esac
}}

cmd mkdir $mkdir -p "$(echo $* | tr ' ' '\ ')"
cmd edit $${EDITOR:-nvim} $*


cmd delete ${{
	clear; tput cup $(($(tput lines)/1)); tput bold
	set -f ; clear ; echo -e "\\n\\n" ; printf "%s\n\t" "$fx" ;
  echo -en \\n\\n"$(tput bold; tput setaf 6)   Delete selected items?$(tput setaf 6) $(tput setaf 1)($(tput setaf 3)Y/n$(tput setaf 1)) $(tput setaf 6): "$(tput setaf 1)""; read ans
    [[ $ans = "y" ]] || [[ $ans = Y ]] || [[ $ans = "" ]] || [[ $ans = yes ]] ||
      [[ $ans = Yes ]] || [[ $ans = YES ]] && rm -rf -- $fx ; tput sgr0 || exit 0 ; tput sgr0
}}

cmd rootdel ${{
  clear; tput cup $(($(tput lines)/1)); tput bold
	set -f ; clear ; echo -e "\\n\\n" ; printf "%s\n\t" "$fx" ;
  echo -en \\n\\n"$(tput bold; tput setaf 6)   ARE YOU SURE YOU WANT TO DELETE THE SELECTED ITEMS?$(tput setaf 6) $(tput setaf 1)($(tput setaf 3)y/N$(tput setaf 1)) $(tput setaf 6): "$(tput setaf 1)""; read ans
    [[ $ans = "y" ]] || [[ $ans = Y ]] || [[ $ans = yes ]] || [[ $ans = Yes ]] ||
      [[ $ans = YES ]] && sudo rm -rf -- $fx ; tput sgr0 || exit 0 ; tput sgr0
}}

cmd symlink ${{
	clear ; dest="$PWD" ; for x in $fx; do
	  eval ln -sf \"$x\" \"$dest\"
  done && lf -remote "send $id unselect" ; notify-send "🔗 File(s) symlinked".\
  "File(s) symlinked to $dest."
}}

cmd symlink_rel ${{
	clear ; dest="$PWD" ; for x in $fx; do
	  eval ln -srf \"$x\" \"$dest\"
  done && lf -remote "send $id unselect" ; notify-send "🔗 File(s) symlinked".\
  "File(s) symlinked to $dest."
}}

cmd symlink_hard ${{
	clear ; dest="$PWD" ; for x in $fx; do
	  eval ln -f \"$x\" \"$dest\"
  done && lf -remote "send $id unselect" ; notify-send "🔗 File(s) symlinked".\
  "File(s) hard linked to $dest."
}}

cmd cloneRepo ${{
  clear; tput cup $(($(tput lines)/3)); fullurl=$(mktemp) ; trap 'rm -f $fullurl' \
  EXIT INT TERM QUIT STOP HUP ERR ; set -f ; url=$(dialog --title "Git Clone" --inputbox \
  " Please enter the URL of the repo you would like to clone: " 10 65 3>&1 1>&2 2>&3 3>&1 &&
  notify-send "📦Cloning repo" "You will be notified upon completion.") ; echo $url|read url
  echo "$(echo $url|sed 's/http:\/\///g;s/https:\/\///g')">$fullurl && url=$(cat $fullurl) &&
  notify='📦Repo successfully cloned into' ; eval $(git clone git@$url >/dev/null 2>&1 &&
  notify-send "$notify $(basename $url)" || git clone https://$url >/dev/null 2>&1 &&
  notify-send "$notify $(basename $url .git)" || notify-send "📦 Repo clone failed.") & disown
}}


cmd fzfp ${{
  BAT="bat -pn --style=numbers,grid --color=always"
  FROM=`echo ${*:-$PWD} | sed 's/\/$//'`

  PREVIEW="file {} | grep -q 'ASCII text' || file {} | grep -q 'Unicode text' && $BAT {} 2>/dev/null && return 0 ||
    echo {} | sed 's/\/$//' | xargs file | grep -q 'symbolic link' && lolcat <<< 'SYMBOLIC LINK.' && return 0 ||
    file {} | grep -q 'directory' && lolcat <<< 'DIRECTORY - LISTING:' && ls -1 ${FROM}/{} 2>/dev/null | $BAT 2>/dev/null && return 0 ||
    lolcat <<< 'BINARY FILE.'"

    filePath(){
      path=$(fzf --multi --ansi --no-sort \
        --prompt="What are you looking for ? : " \
        --header " " \
        --height 99% \
        --preview="$PREVIEW" \
        --preview-window=60% |
        xargs realpath 2>/dev/null) || return 1
    }

    filePath || exit 0
    echo $path | xsel -b && notify-send "✅File path coppied to clipboard" ||
    [[ -f `xsel -bo` ]] && [[ -n ${EDITOR} ]] && $EDITOR `xsel -bo` || exit 0
}}

cmd modeChange ${{
  sudo chmod -R $1 $fx &&
    for x in ${fx[@]}; do
      notify-send "🔒 Permission of $(basename ${x}) changed to $1"
    done ; lf -remote "send $id unselect"
}}


cmd nodeAction ${{

  nodeaction(){
    cat << EOF


      [38;5;44m [38;5;44m [38;5;44m [38;5;44m [38;5;44m_[38;5;44m [38;5;44m [38;5;44m [38;5;44m_[38;5;44m_[38;5;44m [38;5;43m [38;5;49m [38;5;49m [38;5;49m [38;5;49m [38;5;49m [38;5;49m [38;5;49m [38;5;49m [38;5;49m_[38;5;49m_     [38;5;214m [38;5;214m [38;5;214m_[38;5;214m_[38;5;214m_[38;5;214m [38;5;214m [38;5;214m [38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;209m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m [38;5;204m [38;5;198m_[38;5;198m [38;5;198m [38;5;198m [38;5;198m_[38;5;198m_
      [38;5;44m [38;5;44m [38;5;44m [38;5;44m/[38;5;44m [38;5;44m|[38;5;44m [38;5;44m/[38;5;43m [38;5;49m/[38;5;49m_[38;5;49m_[38;5;49m_[38;5;49m [38;5;49m [38;5;49m_[38;5;49m_[38;5;49m_[38;5;49m_[38;5;48m/[38;5;48m [38;5;48m/[38;5;48m_[38;5;48m_   [38;5;214m [38;5;214m/[38;5;214m [38;5;214m [38;5;214m [38;5;208m|[38;5;208m [38;5;208m/[38;5;208m [38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m/[38;5;209m_[38;5;203m [38;5;203m [38;5;203m_[38;5;203m_[38;5;203m/[38;5;203m [38;5;203m [38;5;203m_[38;5;203m/[38;5;203m [38;5;203m_[38;5;204m_[38;5;198m [38;5;198m\[38;5;198m/[38;5;198m [38;5;198m|[38;5;198m [38;5;198m/[38;5;198m [38;5;198m/
      [38;5;44m [38;5;44m [38;5;44m/[38;5;44m [38;5;44m [38;5;43m|[38;5;49m/[38;5;49m [38;5;49m/[38;5;49m [38;5;49m_[38;5;49m_[38;5;49m [38;5;49m\[38;5;49m/[38;5;49m [38;5;48m_[38;5;48m_[38;5;48m [38;5;48m [38;5;48m/[38;5;48m [38;5;48m_[38;5;48m [38;5;48m\[38;5;214m [38;5;214m [38;5;214m/[38;5;214m [38;5;208m/[38;5;208m|[38;5;208m [38;5;208m|[38;5;208m/[38;5;208m [38;5;208m/[38;5;208m [38;5;208m [38;5;209m [38;5;203m [38;5;203m [38;5;203m/[38;5;203m [38;5;203m/[38;5;203m [38;5;203m [38;5;203m/[38;5;203m [38;5;203m/[38;5;203m/[38;5;204m [38;5;198m/[38;5;198m [38;5;198m/[38;5;198m [38;5;198m/[38;5;198m [38;5;198m [38;5;198m|[38;5;198m/[38;5;199m [38;5;199m/
      [38;5;44m [38;5;44m/[38;5;43m [38;5;49m/[38;5;49m|[38;5;49m [38;5;49m [38;5;49m/[38;5;49m [38;5;49m/[38;5;49m_[38;5;49m/[38;5;49m [38;5;48m/[38;5;48m [38;5;48m/[38;5;48m_[38;5;48m/[38;5;48m [38;5;48m/[38;5;48m [38;5;48m [38;5;84m_[38;5;83m_[38;5;83m/[38;5;214m [38;5;208m/[38;5;208m [38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m [38;5;208m/[38;5;208m [38;5;208m/[38;5;209m_[38;5;203m_[38;5;203m_[38;5;203m [38;5;203m [38;5;203m/[38;5;203m [38;5;203m/[38;5;203m [38;5;203m_[38;5;203m/[38;5;203m [38;5;204m/[38;5;198m/[38;5;198m [38;5;198m/[38;5;198m_[38;5;198m/[38;5;198m [38;5;198m/[38;5;198m [38;5;198m/[38;5;199m|[38;5;199m [38;5;199m [38;5;199m/
      [38;5;49m/[38;5;49m_[38;5;49m/[38;5;49m [38;5;49m|[38;5;49m_[38;5;49m/[38;5;49m\[38;5;49m_[38;5;49m_[38;5;48m_[38;5;48m_[38;5;48m/[38;5;48m\[38;5;48m_[38;5;48m_[38;5;48m,[38;5;48m_[38;5;48m/[38;5;84m\[38;5;83m_[38;5;83m_[38;5;83m_[38;5;83m/ [38;5;208m/[38;5;208m_[38;5;208m/[38;5;208m [38;5;208m [38;5;208m|[38;5;208m_[38;5;209m\[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m/[38;5;203m [38;5;203m/[38;5;203m_[38;5;203m/[38;5;203m [38;5;203m/[38;5;204m_[38;5;198m_[38;5;198m_[38;5;198m/[38;5;198m\[38;5;198m_[38;5;198m_[38;5;198m_[38;5;198m_[38;5;198m/[38;5;199m_[38;5;199m/[38;5;199m [38;5;199m|[38;5;199m_[38;5;199m/
      [0m
EOF
  }

  instructions(){

    nodeaction

    echo -e "\n        \e[33m(\e[31m↑\e[33m)\n"
    echo -e "   \e[33m(\e[31m←\e[33m)  \e[32m[\e[34m*\e[32m]\e[33m  (\e[31m→\e[33m)   \e[36mSelect Further Action:\e[0m\n"
    echo -en "        \e[33m(\e[31m↓\e[33m)\n\n  "

    escape_char=$(printf "\u1b")
    read -rsn1 mode

    [[ $mode == $escape_char ]] && read -rsn2 mode || mode=$mode

  }

  getPKGs(){
    echo -en "\n\e[1,33m [+] \e[36m Enter package name(s): \e[0m" ; read pkgs
  }

  schemes=(
    '--color=fg:#F92672,bg:#272822,hl:#A6E22E,fg+:#FD971F,bg+:#272822,hl+:#A6E22E'
    '--color=fg:#66D9EF,bg:#272822,hl:#F92672,fg+:#A6E22E,bg+:#272822,hl+:#F92672'
    '--color=fg:#F92672,bg:#272822,hl:#A6E22E,fg+:#66D9EF,bg+:#272822,hl+:#A6E22E'
    '--color=fg:#F95700,bg:#272822,hl:#E3E3E3,fg+:#246A73,bg+:#2B2B27,hl+:#F2CF1D'
    '--color=fg:#F95700,bg:#272822,hl:#E3E3E3,fg+:#F2CF1D,bg+:#2B2B27,hl+:#246A73'
    '--color=fg:#48E5C2,bg:#272822,hl:#F72585,fg+:#FF61A6,bg+:#2B2B27,hl+:#8D3B72'
    '--color=fg:#CB997E,bg:#272822,hl:#3E4C59,fg+:#2E8B57,bg+:#2B2B27,hl+:#6B4226'
    '--color=fg:#4EA8DE,bg:#272822,hl:#F6CD61,fg+:#FFC107,bg+:#2B2B27,hl+:#393E46'
    '--color=fg:#F72585,bg:#272822,hl:#7209B7,fg+:#F4F1BB,bg+:#2B2B27,hl+:#560BAD'
    '--color=fg:#E63946,bg:#272822,hl:#F1FAEE,fg+:#A8DADC,bg+:#2B2B27,hl+:#457B9D'
    '--color=fg:#2A9D8F,bg:#272822,hl:#E76F51,fg+:#F4F1DE,bg+:#2B2B27,hl+:#264653'
    '--color=fg:#1B9AAA,bg:#272822,hl:#FEF9EF,fg+:#D4A5A5,bg+:#2B2B27,hl+:#392F5A'
    '--color=fg:#D00000,bg:#272822,hl:#FFBA08,fg+:#EAE2B7,bg+:#2B2B27,hl+:#540B0E'
    '--color=fg:#9A031E,bg:#272822,hl:#FAFCC2,fg+:#03C4A1,bg+:#2B2B27,hl+:#D4A5A5'
    '--color=fg:#0A9396,bg:#272822,hl:#C9CB3C,fg+:#E9D8A6,bg+:#2B2B27,hl+:#EE6C4D'
  )

  BAT="bat -pn --style=numbers,grid,changes --italic-text=always --paging=always --pager='less -R' --color=always --line-range :500 --decorations=always"
  FROM=`echo ${*:-$PWD} | sed 's/\/$//'`

  selectNodeScript(){

    srtdir="$FROM"
    while true; do
      choices=()
      [ "$srtdir" != "/" ] && choices+=("..")
      choices+=($(ls -1 "$srtdir"))

      RandomColor=${schemes[$RANDOM % ${#schemes[@]} ]}

      file=$(printf "%s\n" "${choices[@]}" |
        fzf --ansi --no-sort --cycle --pointer "▶" \
          $RandomColor \
          --prompt="Select JavaScript File: " \
          --info=inline --padding=1 --border --margin=1 \
          --header "
Enter : proceed
Space : toggle sort
Tab   : toggle preview
          " \
          --height 60% \
          --preview-window right:60% \
          --bind "tab:toggle-preview,space:toggle-sort" \
          --preview "[ -f $srtdir/{} ] && $BAT $srtdir/{} || { echo -e '\n\e[32mDirectory Listing:\e[0m\n'; ls -1 $srtdir/{}; } | $BAT"
      )

      [ -z "$file" ] && exit 0

      [ "$file" == ".." ] && srtdir=`realpath "$srtdir"` ||
      [ -d "$srtdir/$file" ] && srtdir=`realpath "$srtdir/$file"` ||
      [ -f "$srtdir/$file" ] && echo "$file" | grep -qE ".(js|mjs)$" && {
        file=`realpath "$srtdir/$file"`
        [ -f "$file" ] && return 0
      } || {
        [ -f "$srtdir/$file" ] && {
          return 1
        }
      }
    done
  }


  runScript(){
    yarn $@ && echo -en "\npress any key to continue..." && read -n 1 -s -r -p "" || return 1
  }

  exeScript(){
    node $@ && echo -en "\npress any key to continue..." && read -n 1 -s -r -p "" || return 1
  }


  NodeManager(){

    instructions

    case $mode in
      '[A'|'u'|'s') npm init -y && notify-send "📦 Package.json created" ;;
      '[D') clear && getPKGs && for x in ${pkgs[@]}; do eval "runScript add $x >/dev/null 2>&1 && notify-send '✅ $x installed'" ; done ;;
      '[B'|'y') clear && { selectNodeScript && exeScript $file; } || notify-send "📦 Failed to run script" "'$file' is not a valid JavaScript file." ;;
      '[C') clear && notify-send "📦 Starting Server..." && runScript start ;;
      'h') notify-send "📦 Starting Server in Development Mode..." && runScript dev ;;
      'd'|'m') notify-send "📦 Starting Server in Development Mode..."&& runScript dev >/dev/null 2>&1 & disown ;;
      'b'|'n') notify-send "📦 Building Project..." && runScript build ;;
      'i'|'I'|',') clear ; echo -e "\e[0m" && npm init ;;
      'r') clear && getPKGs && for x in ${pkgs[@]}; do runScript remove $x && notify-send "✅ $x removed" ; done ;;
      '.') notify-send "📦 Updating Server..." && runScript && notify-send "📦 Package Base Updated" ;;
      's') clear ; selectNodeScript && exeScript $file ;;
      '/') clear ; notify-send "📦 Starting server on port 5000..." &&  npx serve -l 5000 ;;
      'q') exit 0 ;;
     *) NodeManager ;;
    esac
  }

  clear ; NodeManager

}}

cmd pyAction ${{

  pyaction(){
    cat << EOF


      [38;5;39m [38;5;39m [38;5;39m [38;5;39m [38;5;39m_[38;5;39m_[38;5;38m_[38;5;44m_         [38;5;214m [38;5;214m_[38;5;214m_[38;5;214m_[38;5;214m [38;5;214m [38;5;214m [38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;209m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m [38;5;204m [38;5;198m_[38;5;198m [38;5;198m [38;5;198m [38;5;198m_[38;5;198m_
      [38;5;39m [38;5;39m [38;5;39m [38;5;38m/[38;5;44m [38;5;44m_[38;5;44m_[38;5;44m [38;5;44m\[38;5;44m_[38;5;44m_[38;5;44m [38;5;44m [38;5;44m_[38;5;44m_[38;5;214m[38;5;214m  [38;5;214m/[38;5;214m [38;5;214m [38;5;214m [38;5;208m|[38;5;208m [38;5;208m/[38;5;208m [38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m/[38;5;209m_[38;5;203m [38;5;203m [38;5;203m_[38;5;203m_[38;5;203m/[38;5;203m [38;5;203m [38;5;203m_[38;5;203m/[38;5;203m [38;5;203m_[38;5;204m_[38;5;198m [38;5;198m\[38;5;198m/[38;5;198m [38;5;198m|[38;5;198m [38;5;198m/[38;5;198m [38;5;198m/
      [38;5;38m [38;5;44m [38;5;44m/[38;5;44m [38;5;44m/[38;5;44m_[38;5;44m/[38;5;44m [38;5;44m/[38;5;44m [38;5;44m/[38;5;44m [38;5;43m/[38;5;49m /[38;5;214m /[38;5;214m [38;5;208m/[38;5;208m|[38;5;208m [38;5;208m|[38;5;208m/[38;5;208m [38;5;208m/[38;5;208m [38;5;208m [38;5;209m [38;5;203m [38;5;203m [38;5;203m/[38;5;203m [38;5;203m/[38;5;203m [38;5;203m [38;5;203m/[38;5;203m [38;5;203m/[38;5;203m/[38;5;204m [38;5;198m/[38;5;198m [38;5;198m/[38;5;198m [38;5;198m/[38;5;198m [38;5;198m [38;5;198m|[38;5;198m/[38;5;199m [38;5;199m/
      [38;5;44m [38;5;44m/[38;5;44m [38;5;44m_[38;5;44m_[38;5;44m_[38;5;44m_[38;5;44m/[38;5;44m [38;5;43m/[38;5;49m_[38;5;49m/[38;5;49m [38;5;49m/[38;5;208m /[38;5;208m [38;5;208m_[38;5;208m_[38;5;208m_[38;5;208m [38;5;208m/[38;5;208m [38;5;208m/[38;5;209m_[38;5;203m_[38;5;203m_[38;5;203m [38;5;203m [38;5;203m/[38;5;203m [38;5;203m/[38;5;203m [38;5;203m_[38;5;203m/[38;5;203m [38;5;204m/[38;5;198m/[38;5;198m [38;5;198m/[38;5;198m_[38;5;198m/[38;5;198m [38;5;198m/[38;5;198m [38;5;198m/[38;5;199m|[38;5;199m [38;5;199m [38;5;199m/
      [38;5;44m/[38;5;44m_[38;5;44m/[38;5;44m [38;5;44m [38;5;44m [38;5;43m [38;5;49m\[38;5;49m_[38;5;49m_[38;5;49m,[38;5;49m [38;5;49m/[38;5;214m /[38;5;208m_[38;5;208m/[38;5;208m [38;5;208m [38;5;208m|[38;5;208m_[38;5;209m\[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m_[38;5;203m/[38;5;203m [38;5;203m/[38;5;203m_[38;5;203m/[38;5;203m [38;5;203m/[38;5;204m_[38;5;198m_[38;5;198m_[38;5;198m/[38;5;198m\[38;5;198m_[38;5;198m_[38;5;198m_[38;5;198m_[38;5;198m/[38;5;199m_[38;5;199m/[38;5;199m [38;5;199m|[38;5;199m_[38;5;199m/
      [38;5;44m [38;5;44m [38;5;44m [38;5;43m [38;5;49m [38;5;49m [38;5;49m/[38;5;49m_[38;5;49m_[38;5;49m_[38;5;49m_[38;5;49m/
      [0m
EOF
  }

  instructions(){

    pyaction

    echo -e "\n        \e[33m(\e[31m↑\e[33m)\n"
    echo -e "   \e[33m(\e[31m←\e[33m)  \e[32m[\e[34m*\e[32m]\e[33m  (\e[31m→\e[33m)   \e[36mSelect Further Action:\e[0m\n"
    echo -en "        \e[33m(\e[31m↓\e[33m)\n\n  "

    escape_char=$(printf "\u1b")
    read -rsn1 mode

    [[ $mode == $escape_char ]] && read -rsn2 mode || mode=$mode

  }

  schemes=(
    '--color=fg:#F92672,bg:#272822,hl:#A6E22E,fg+:#FD971F,bg+:#272822,hl+:#A6E22E'
    '--color=fg:#66D9EF,bg:#272822,hl:#F92672,fg+:#A6E22E,bg+:#272822,hl+:#F92672'
    '--color=fg:#F92672,bg:#272822,hl:#A6E22E,fg+:#66D9EF,bg+:#272822,hl+:#A6E22E'
    '--color=fg:#F95700,bg:#272822,hl:#E3E3E3,fg+:#246A73,bg+:#2B2B27,hl+:#F2CF1D'
    '--color=fg:#F95700,bg:#272822,hl:#E3E3E3,fg+:#F2CF1D,bg+:#2B2B27,hl+:#246A73'
    '--color=fg:#48E5C2,bg:#272822,hl:#F72585,fg+:#FF61A6,bg+:#2B2B27,hl+:#8D3B72'
    '--color=fg:#CB997E,bg:#272822,hl:#3E4C59,fg+:#2E8B57,bg+:#2B2B27,hl+:#6B4226'
    '--color=fg:#4EA8DE,bg:#272822,hl:#F6CD61,fg+:#FFC107,bg+:#2B2B27,hl+:#393E46'
    '--color=fg:#F72585,bg:#272822,hl:#7209B7,fg+:#F4F1BB,bg+:#2B2B27,hl+:#560BAD'
    '--color=fg:#E63946,bg:#272822,hl:#F1FAEE,fg+:#A8DADC,bg+:#2B2B27,hl+:#457B9D'
    '--color=fg:#2A9D8F,bg:#272822,hl:#E76F51,fg+:#F4F1DE,bg+:#2B2B27,hl+:#264653'
    '--color=fg:#1B9AAA,bg:#272822,hl:#FEF9EF,fg+:#D4A5A5,bg+:#2B2B27,hl+:#392F5A'
    '--color=fg:#D00000,bg:#272822,hl:#FFBA08,fg+:#EAE2B7,bg+:#2B2B27,hl+:#540B0E'
    '--color=fg:#9A031E,bg:#272822,hl:#FAFCC2,fg+:#03C4A1,bg+:#2B2B27,hl+:#D4A5A5'
    '--color=fg:#0A9396,bg:#272822,hl:#C9CB3C,fg+:#E9D8A6,bg+:#2B2B27,hl+:#EE6C4D'
  )

  getPKGs(){
    echo -en "\n\e[1,33m [+] \e[36m Enter package name(s): \e[0m" ; read pkgs
  }

  BAT="bat -pn --style=numbers,grid,changes --italic-text=always --paging=always --pager='less -R' --color=always --line-range :500 --decorations=always"
  FROM=`echo ${*:-$PWD} | sed 's/\/$//'`

  selectPyScript(){

    srtdir="$FROM"
    while true; do
      choices=()
      [ "$srtdir" != "/" ] && choices+=("..")
      choices+=($(ls -1 "$srtdir"))

      RandomColor=${schemes[$RANDOM % ${#schemes[@]} ]}

      file=$(printf "%s\n" "${choices[@]}" |
        fzf --ansi --no-sort --cycle --pointer "▶" \
          $RandomColor \
          --info=inline --padding=1 --border --margin=1 \
          --prompt="$1" \
          --header "
Enter : proceed
Space : toggle sort
Tab   : toggle preview
          " \
          --height 60% \
          --preview-window right:60% \
          --bind "tab:toggle-preview,space:toggle-sort" \
          --preview "[ -f $srtdir/{} ] && $BAT $srtdir/{} || { echo -e '\n\e[32mDirectory Listing:\e[0m\n'; ls -1 $srtdir/{}; } | $BAT"
      )

      [ -z "$file" ] && exit 1

      [ "$file" == ".." ] && srtdir=`realpath "$srtdir"` ||
      [ -d "$srtdir/$file" ] && srtdir=`realpath "$srtdir/$file"` ||
      [ -f "$srtdir/$file" ] && echo "$file" | grep -q ".py$" && {
        file=`realpath "$srtdir/$file"`
        [ -f "$file" ] && return 0
      } || {
        [ -f "$srtdir/$file" ] && {
          echo "Invalid Selection. $srtdir/$file is not a python script"
          break
        }
      }
    done
  }

  newConda(){
    which mamba >/dev/null 2>&1 && tool=mamba || tool=conda
    $tool create -p ./.🐍 -y -c conda-forge python=3.10 pynvim && notify-send "Conda Environment Created at ./.🐍" || {
      notify-send "🐍 Conda Environment Creation Failed"
      return 1
    }

    [[ -d $HOME/PKG/lfpreviewer ]] && {
      pip install -e $HOME/PKG/lfpreviewer >/dev/null 2>&1 &&
      notify-send "🐍 lfpreviewer Installed"
    }
  }

  runScript(){
    python3 $1 && echo -en "\npress any key to continue..." && read -n 1 -s -r -p "" || return 1
  }

  installPkg(){
    pip install $1 >/dev/null 2>&1 && notify-send "✅ $1 installed" || notify-send "❌ Failed to install $1"
  }

  PyManager(){

    instructions

    case $mode in
        '[A') clear && { which lfp-newPyEnv >/dev/null 2>&1 && lfp-newPyEnv || newConda; } ;;
        '[B') clear && { selectPyScript "Select Python Script: " && runScript $file; } || notify-send "🐍 Failed to run script" "'$file' is not a valid python script" ;;
        '[D') clear && getPKGs && for x in ${pkgs[@]}; do installPkg $x; done ;;
        '[C') clear && [[ -f "main.py" ]] || { notify-send "🐍 No main.py file found"; return 1; } && notify-send "🐍 Running main.py script..." && runScript main.py ;;
        'r') pip3 install -r requirements.txt && notify-send "🐍 Requirements installed" ;;
        'R') clear && { selectPyScript "Select Requirements File: " && pip install -r $file; } && notify-send "🐍 Requirements installed" || notify-send "🐍 Failed to install requirements" "'$file' is not a valid requirements file" ;;
        'q') exit 0 ;;
       *) PyManager ;;
      esac
    }

  clear ; PyManager

}}


cmd newFile ${{
  touch $@ && notify-send "📝 \"${@}\"          Created successfully!"
}}


cmd meChange ${{
  ! sudo chown "nvx1:nvx1" -Rf ${*} && notify-send "🔧 Owning User Changed to nvx1 &&
    notify-send "🔧 Ownership Change Failed"
    lf -remote "send $id unselect"
}}

cmd mpvPlay ${{
  mpv --audio-display=no $f >/dev/null 2>&1 &
  notify-send "🎶  Playing $(basename $f)"
}}

#cmd newFile ${{
#  file=$(dialog --title ' ~ New file ~ ' --inputbox \
#  " Enter a name for the file: " 10 60 3>&1 1>&2 2>&3 3>&1) && echo $file|read file
#  [ -e $file ] && notify-send "❌ Error, $file already exists in this directory."&& exit 1 ||
#  touch $file && notify-send "📝 File: \"${file}\"          Created successfully!"
#}}

cmd pushTo ${{
  dest="$(echo `git remote`|xargs -n1|fzf \
    --no-extended \
    --layout reverse \
    --header " " \
    --prompt " Push to :" \
    --no-info \
    --disabled \
    --border rounded \
    --bold)" || exit 1

  git add .
  git commit -S -m "Auto-Commit Update `date '+%d.%m.%Y - %T'`" || true
  git push $dest
}}


cmd sudoCP ${{
dest=$PWD
    for x in ${fx[@]}; do
      sudo cp -r $x $dest &&
      notify-send "📋 $(basename ${x}) copied to ${dest}" ||
      notify-send "❌ $(basename ${x}) could not be copied to ${dest}." "Please ensure you sudo privileges."
    done ; lf -remote "send $id unselect"
}}

cmd sudoMV ${{
dest=$PWD
    for x in ${fx[@]}; do
      sudo mv $x $dest &&
      notify-send "🚚 $(basename ${x}) moved to ${dest}" ||
      notify-send "❌ $(basename ${x}) could not be moved to ${dest}." "Please ensure you sudo privileges."
    done ; lf -remote "send $id unselect"
}}

cmd makeTemp ${{
tmpd=$(echo $$) ; eval $(mkdir $tmpd && cd $tmpd && notify-send "🔧 Temp dir created." ||
    notify-send "❌ Could not create temp dir.")
}}

cmd cpPath ${{
	realpath ${f:-.} | xargs echo -n | xsel -ib
  notify-send "📋 Copied path" "\"$(basename $f)\" path coppied to clipboard."
}}

cmd browserOpen ${{
  ${BROWSER:-firefox} $fx >/dev/null 2>&1 || ${BROWSER:-firefox} $f || notify-send "❌No supported browser detected." \
  "Please set the BROWSER env variable."
}}

cmd ext ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f ; printf "%s\n\t" "$fx"
	printf "extract?[Y/n]"
	read ans ; anss=( n N NO nO No )
  SUCC(){ notify-send "🔧$(basename ${ARG}) extracted successfully!" ; }
  FAIL(){ notify-send "❌ '$(basename ${ARG})' could not be extracted via $(basename $0)." ; }
	[[ "${anss[*]}" != "$ans" ]] &&
    for ARG in $fx ; do
    [ -f ${ARG} ] && case ${ARG} in
      *.bz2)       bunzip2 ${ARG} && SUCC || FAIL ;;
      *.rar)       unrar x ${ARG} && SUCC || FAIL ;;
      *.tar)       tar xpf ${ARG} && SUCC || FAIL ;;
      *.tar.*)     tar xpf ${ARG} && SUCC || FAIL ;;
      *.tbz2)      tar xjf ${ARG} && SUCC || FAIL ;;
      *.gz)        gunzip  ${ARG} && SUCC || FAIL ;;
      *.tgz)       tar xzf ${ARG} && SUCC || FAIL ;;
      *.zip)       unzip ${ARG} && SUCC || FAIL ;;
      *.whl)       unzip ${ARG} && SUCC || FAIL ;;
      *.Z)         uncompress ${ARG} && SUCC || FAIL ;;
      *.7z)        7z x ${ARG} && SUCC || FAIL ;;
      *.deb)       ar x ${ARG} && SUCC || FAIL ;;
      *)           notify-send "❌ '$(basename ${ARG})' could not be extracted with lfp." ;
    esac ; done
    lf -remote "send $id unselect"
}}

cmd newTernPwd ${{
  ${TERMINAL:-st} & disown
}}


cmd setbg "$1"
cmd bulkrename $vidir

cmd ocr ${{
  result=${result:-}
  for x in ${fx[@]}; do
    node /usr/share/lfp/ocr/ocr.js $x
    result="$(cat << EOF
$result



$(basename $x):
----------------
$(xsel -bo)
EOF
    )"
  done
  notify-send "🔧 OCR Complete" "Text copied to clipboard."
  [[ -n $result ]] && echo -e "$result" | awk '/./,EOF' | tac | awk '/./,EOF' | tac | xsel -bi
  [[ -n $result ]] && echo -e "$result" | awk '/./,EOF' | tac | awk '/./,EOF' | tac | less --use-color +%
}}



  #-----------------------#
 #  ~  B I N D I N G S  ~  #
  #-----------------------#


# Basic Bindings:

map gh
map g top
map c cut
map d delete
map - delete
map a rename
map <delete> delete
map <c-r> reload
map <backspace2> set hidden!
map <enter> shell
map o ocr


# LFP-Custom Functions:

map m mpvPlay
map b browserOpen
map f fzfp
map B bulkrename
map E ext
map C cpPath
map M sudoMV
map D rootdel
map L symlink
map R symlink_rel
map H symlink_hard
map P sudoCP
map <c-g> pushTo
map <c-b> !xwallpaper --stretch $f
map <c-t> makeTemp
map <c-^> modeChange 644 # Ctrl + 6
map <c-_> modeChange 755 # Ctrl + 7
map = meChange $fx
map I newTernPwd
map . nodeAction
map , pyAction
map <c-c> cloneRepo


# Commmand Oriented Bindings:

map x !$f
map s !${EDITOR:-nvim} $f
map z !sxiv $f
map Z !sxiv *
map V !mpv *
map S !sudo ${EDITOR:-nvim} $f
map X !chmod +x $f ; !$f
map <c-v> !mpv --shuffle *
map <a-v> !mpv $f
map <c-s> :!${EDITOR:-nvim} $?
map <c-e> :!chmod +x $f
map <c-y> :!mpv --shuffle $PWD
map <c-y> :!mpv --shuffle $PWD
map A !chmod +x *


# Push Bindings:

map e push :edit<space>
map t push :newFile<space>
map n push :mkdir<space>
map N push :sudo mkdir<space>


