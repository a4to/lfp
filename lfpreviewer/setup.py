#!/usr/bin/env python3

import setuptools
import glob
from setuptools import setup, Extension

X = Extension(
    'lfpreviewer.X',
    sources=glob.glob('lfpreviewer/X/*.c'),
    include_dirs=['/usr/include/X11', 'lfpreviewer/X/'],
    libraries=['X11', 'Xext', 'XRes'],
)

setuptools.setup(
    name='lfpreviewer',
    version='1.0.1',
    ext_modules=[X],
    description='Image Previewer for LFP',
    license='MIT/X Consortium License',
    python_requires='>=3.6',
    install_requires=['docopt', 'attrs>=18.2.0', 'pillow', "setuptools>=42", "wheel"],
    sources=glob.glob('lfpreviewer/X/*.c'),
    include_dirs=['lfpreviewer/X/'],
    include_package_data=True,
    package_data={ '': ['*.sh'] },
    packages=setuptools.find_packages(),
    entry_points={ 'console_scripts': [
        'lfpreviewer=lfpreviewer.__main__:main'
    ]},
    classifiers=[
        'Environment :: Console',
        'Environment :: X11 Applications',
        'Operating System :: POSIX :: Linux'
    ]
)
